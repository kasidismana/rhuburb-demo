output = {
  metadata: {
    soundFile: "D:\\AI\\projects\\DSS\\rhuburb-demo\\rec\\mouth.ogg",
    duration: 0.96,
  },
  mouthCues: [
    { start: 0.0, end: 0.3, value: "X" },
    { start: 0.3, end: 0.42, value: "A" },
    { start: 0.42, end: 0.54, value: "C" },
    { start: 0.54, end: 0.68, value: "E" },
    { start: 0.68, end: 0.89, value: "B" },
    { start: 0.89, end: 0.96, value: "X" },
  ],
};
